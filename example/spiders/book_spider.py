# -*- coding: utf-8 -*-
import scrapy
from .. items import PaperItem 

class BooksSpider(scrapy.Spider):
    
    name = "books"
    # start_urls = ['http://www.lis.ac.cn/CN/article/showTenYearOldVolumn.do']
    start_urls = ['http://www.lis.ac.cn/CN/volumn/volumn_630.shtml']
    
    def parse1(self, response):
        next_urls = response.css('td[bgcolor="#CCCCCC"] > a')
        for next_url in next_urls:
            url = response.urljoin(next_url.css('::attr("href")').extract_first())
            yield scrapy.Request(url, callback=self.parse_issue_list)
    
    def parse_issue_list(self, response):
        next_urls = response.css('td.J_WenZhang[height="20"] > a')
        for next_url in next_urls:
            url = response.urljoin(next_url.css('::attr("href")').extract_first())
            yield scrapy.Request(url, callback=self.parse_issue_list)

    def parse(self, response):
        next_urls = response.css('a.J_VM')
        for next_url in next_urls:
            if '摘要' in next_url.css('u::text').extract_first():
                url = response.urljoin(next_url.css('::attr("href")').extract_first())
                yield scrapy.Request(url, callback=self.parse_paper_detail, priority=1)
    
    def parse_paper_detail(self, response):
        title = response.xpath('//span[@class="J_biaoti"]/text()').extract_first()
        paper = PaperItem()
        paper['title'] = title
        yield paper
